<?php

namespace App\Http\Controllers;

use App\ProductModel;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getProduct(){
        return response()->json(ProductModel::get());
    }

    public function deleteProduct(Request $request){
        $a = ProductModel::where(['id'=>$request->id])->delete();

        $data = ProductModel::get();

        return response()->json($data);
    }

    public function updateProduct(Request $request){
        $a = ProductModel::where(['id'=>$request->id])->update(['name'=>$request->name]);

        $data = ProductModel::get();

        return response()->json($data);
    }

    public function insertProduct(Request $request){
        $data = new ProductModel();
        $data->name = $request->name;
        $data->price = $request->price;
        $data->stock = $request->stock;
        $data->save();

        $data = ProductModel::get();

        return response()->json($data);
    }

}
