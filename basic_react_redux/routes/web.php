<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/getProduct','ProductController@getProduct');

Route::post('/delete-product','ProductController@deleteProduct');

Route::post('/update-product','ProductController@updateProduct');

Route::post('/insert-product','ProductController@insertProduct');