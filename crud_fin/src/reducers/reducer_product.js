import {FETCH_PRODUCT,DELETE_PRODUCT,UPDATE_PRODUCT,INSERT_PRODUCT} from '../action/index';

const INITIAL_STATE = {all : [], product : null};

export default function(state = INITIAL_STATE, action){

    switch(action.type){
        case FETCH_PRODUCT : 
            return {...state , all : action.payload.data};
        case DELETE_PRODUCT :
            return {...state , all : action.payload.data};
        case UPDATE_PRODUCT : 
            return {...state , all : action.payload.data};
        case INSERT_PRODUCT :
            console.log('asd');
            return {...state , all : action.payload.data};
        default :
            return state;
    }

}