import React, {Component} from 'react';
import {connect} from 'react-redux';

import {deleteProduct,updateProduct} from '../action/index';

class ListProduct extends Component{

    constructor(props){
        super(props);

        this.state = {type : 'step1', name : '', id : ''};
        this.onClickUpdate = this.onClickUpdate.bind(this);
        this.renderListProduct = this.renderListProduct.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    onClickUpdate(id){
        if(this.state.type == 'step1'){
            this.setState({type : 'step2',id : id});
        }else if(this.state.type == 'step2'){
            this.setState({type : 'step3',id : id});
        }else if(this.state.type == 'step3'){
            this.setState({type : 'step1'});
        }
    }

    onInputChange(event){
        this.setState({name : event.target.value});
    }

    renderListProduct(){
        if(this.state.type == 'step1'){
            return (
                <tr>
                    <td>{this.props.data.id}</td>
                    <td>{this.props.data.name}</td>
                    <td>{this.props.data.price}</td>
                    <td>{this.props.data.stock}</td>
                    <td> <button onClick = {()=>this.props.deleteProduct(this.props.data.id)}> Delete </button> </td>
                    <td><button onClick = {() => this.onClickUpdate(this.props.data.id)} > Update </button></td>
                </tr>
            );
        }else if(this.state.type == 'step2'){
            return(
                <tr>
                    <td>{this.props.data.id}</td>
                    <td><input type="text" value={this.state.name} onChange = {this.onInputChange} /></td>
                    <td>{this.props.data.price}</td>
                    <td>{this.props.data.stock}</td>
                    <td> <button onClick = {()=>this.props.deleteProduct(this.props.data.id)}> Delete </button> </td>
                    <td><button onClick = {() => this.onClickUpdate(this.props.data.id)} > Update </button></td>
                </tr>
            );
        }else if(this.state.type == 'step3'){
            this.props.updateProduct(this.state.id,this.state.name);
            this.setState({type : 'step1'});
        }
    }

    render(){
        return(
            <tbody>
                {this.renderListProduct()}
            </tbody>
        );
    }
}

export default connect(null,{deleteProduct,updateProduct})(ListProduct);