import React, { Component } from 'react';
import {getProduct,insertProduct} from '../action';
import {reduxForm} from 'redux-form';

import ListProduct from './list_product';

class App extends Component {

  componentWillMount(){
    this.props.getProduct();
  }

  renderProduct(){
    if(this.props.products.all.length != 0){
        return this.props.products.all.map((product)=>{
          return (
            <ListProduct data={product} key={product.id} />
          );
        });
    }else{
      return (<tbody><tr>
        <td>Loading...</td>
      </tr></tbody>);
    }
  }

  onSubmit(props){
    this.props.insertProduct(props);
  }

  render() {
    const {fields : {name,price,stock}, handleSubmit} = this.props;
    return (
      <div>

        <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          Product Name <input type="text" {...name} /><br/>
          Price <input type="text" {...price} /> <br/>
          Stock <input type="text" {...stock}/> <br/>
          <div>{name.touched ? name.error : ''} {price.touched ? price.error : ''} {stock.touched ? stock.error : ''}</div>
          <button> Insert </button>
        </form>

        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Product Name</th>
              <th>Price</th>
              <th>Stock</th>
            </tr>
          </thead>
          {this.renderProduct()}
        </table>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {products : state.products};
}

function validate(values){
  const err = {};

  if(!values.name){
    err.name = 'product name can not be empty';
  }
  if(!values.price){
    err.price = 'product price can not be empty';
  }
  if(!values.stock){
    err.stock = 'product stock can not be empty';
  }
  return err;
}

export default reduxForm({
    form : 'InsertForm',
    fields : ['name','price','stock'],
    validate
}, mapStateToProps, {getProduct, insertProduct})(App);