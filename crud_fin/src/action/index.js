import axios from 'axios';

const ROOT_URL = 'http://localhost:8000/';
export const FETCH_PRODUCT = 'FETCH_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const INSERT_PRODUCT = 'INSERT_PRODUCT';

export function getProduct(){
    const data = axios.get(ROOT_URL+'getProduct');
    return {
        type : FETCH_PRODUCT,
        payload : data
    };
}

export function deleteProduct(id){
    const data = axios.post(ROOT_URL+'delete-product',{id});
    return{
        type : DELETE_PRODUCT,
        payload : data
    };
}

export function updateProduct(id,name){
    const data = axios.post(ROOT_URL+'update-product',{id,name});
    return {
        type : UPDATE_PRODUCT,
        payload : data
    };
}

export function insertProduct(props){
    const data = axios.post(ROOT_URL+'insert-product',props);
    return {
        type : INSERT_PRODUCT,
        payload : data
    }
}